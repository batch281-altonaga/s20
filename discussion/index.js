/* While Loop 

    - it evaluates a condition, if returned true, it will execute statements as long as the condition is satisfied. If at first the condition is not satisfied, no statement will be executed.

    Syntax:
        while (expression/conditions){
            statement/s;
        }
*/

let count = 5;

while (count !== 0) {
  console.log("While: " + count);
  count--;
}
/* 
    count: 5, 4, 3, 2, 1, 0 ( -- stop -- )
    Console:
        While: 5
        While: 4
        While: 3
        While: 2
        While: 1
*/

console.log("\n");

// Mini activity: Using while loop, display numbers 1-5

let x = 1;

while (x <= 5) {
  console.log("While: " + x);
  x++;
}

// Do-while Loop
/* 
    - iterates statements within a number of times based on a condition. However, if the condition was not satisfied at first, one statement will be executed.
    Syntax:
        do {
            statement/s;
            while(expression/condition);
        }
*/

// let number = Number(prompt("Give me a number: "));

// do {
//   console.log("Do while: " + number);
//   number += 1;
// } while (number < 10);

let even = 2;
do {
  console.log(even);
  even += 2;
} while (even <= 10);

console.log("\n");

// For Loop
/* 
    - a looping construct that is more flexible than other loops. It consists of three parts:
    1. Initialization
    2. Expression/Condition
    3. Final Expression / Stop Expression

    Syntax: 
        for (initialization; control expression; update){
            statement/s;
        }
*/

for (let count = 0; count <= 20; count++) {
  console.log(count);
}
/* 
    count = 0;
    Console:
        0
        1
        2
        3
        ...
        20
*/

let myString = "tanli";

// .length counts the num of chars of a string, including space.
console.log(myString.length);

// Accessing elements of a string
console.log(myString[0]);
console.log(myString[2]);
console.log(myString[3]);

console.log("\n");
for (let x = 0; x < myString.length; x++) {
  console.log(myString[x]);
}

console.log("\n");

let myName = "AlEx";
for (let i = 0; i < myName.length; i++) {
  if (
    myName[i].toLowerCase() == "a" ||
    myName[i].toLowerCase() == "e" ||
    myName[i].toLowerCase() == "i" ||
    myName[i].toLowerCase() == "o" ||
    myName[i].toLowerCase() == "u"
  ) {
    console.log(3);
  } else {
    console.log(myName[i]);
  }
}

// Continue and Break Statements
for (let count = 0; count <= 20; count++) {
  if (count % 2 === 0) {
    // Tells the code to continue to the next iteration of the loop
    continue;
  }

  console.log("Continue and Break: " + count);

  // If the current value of count is greater than 10, stops the loop
  if (count > 10) {
    break;
    // tells the code to stop the loop even if the condition of the loop defines that it should execute
  }
}

// count: 10, 11

let name = "alexandro";

for (let i = 0; i < name.length; i++) {
  console.log(name[i]);
  if (name[i].toLowerCase() === "a") {
    console.log("Continue to the next iteration");
    continue;
  }

  if (name[i] == "d") {
    break;
  }
}
